import React from 'react';
import Perfil1 from './imagen1';
import Perfil2 from './Imagen2';
import Perfil3 from './Imagen3';
import "./Card.css";

const Tarjet=(props)=>{
    return(
        <div className="perfiles">
            <div className="principal tarjeta1">
                <div className="perfil">
                    <Perfil1 />
                    <div>
                    <p className="nombre">Colton Smith</p>
                    <p className="nombre">Verified Buyer</p>
                    </div>

                </div>
                <p className="action">
                "We needed the same printed design as the one we had ordered a week prior. Not only did they find the original order, but we also received it in time. Excellent!"
                </p>
            </div>
            <div className="principal tarjeta2">
                <div className="perfil">
                    <Perfil2 />
                    <div>
                    <p className="nombre">Irene Roberts</p>
                    <p className="nombre">Verified Buyer</p>
                    </div>

                </div>
                <p className="action">
                "We needed the same printed design as the one we had ordered a week prior. Not only did they find the original order, but we also received it in time. Excellent!"
                </p>
            </div>
            <div className="principal tarjeta3">
                <div className="perfil">
                    <Perfil3 />
                    <div>
                    <p className="nombre">Anne Wallace</p>
                    <p className="nombre">Verified Buyer</p>
                    </div>

                </div>
                <p className="action">
                "We needed the same printed design as the one we had ordered a week prior. Not only did they find the original order, but we also received it in time. Excellent!"
                </p>
            </div>

        </div>

    )

}
export default Tarjet;