import React from "react"
import PropTypes from "prop-types"
// import React, { Fragment } from 'react';

function PrimeraApp(props) {
  return (
    <>
      <h1>{props.titulo}</h1>
      <p>{props.subTitulo}</p>
    </>
  )
}

export default PrimeraApp
