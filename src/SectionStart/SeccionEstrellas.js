import React from 'react';
import "./Estrellas.css";
import "./texto.css";
import Start from './estrella';

const Text=(props)=>{
    return(
        <div className="contenedor_padre">
            <div className="contenedor">
                <h1>10,000+ of our users love our products.</h1>
                <p>We only provide great products combined with excellent customer service. See what our satisfied customers are saying about our services.</p>
            </div>
            <div className="resultados">
                <div className="contenedor1">
                    <div className="start">
                        <Start />
                        <Start />
                        <Start />
                        <Start />
                        <Start />

                    </div>
                    <p>Rated 5 Stars in Reviews</p>



                </div>
            </div>
        </div>
    );
}
export default Text;